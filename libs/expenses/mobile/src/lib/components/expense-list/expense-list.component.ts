import { CommonModule } from '@angular/common';
import { Component, Input, OnChanges } from '@angular/core';
import { Expense } from '@dinaro/expenses';
import { AutoAnimateModule } from '@dinaro/ui';
import { IonicModule, InfiniteScrollCustomEvent } from '@ionic/angular';
import { ExpenseItemComponent } from '../expense-item/expense-item.component';

@Component({
  selector: 'dinaro-expense-list',
  standalone: true,
  imports: [AutoAnimateModule, CommonModule, ExpenseItemComponent, IonicModule],
  templateUrl: 'expense-list.component.html',
  styleUrls: ['expense-list.component.scss'],
})
export class ExpenseListComponent implements OnChanges {
  @Input() expenses: Expense[] | null | undefined;
  @Input() loading = false;

  renderedExpenses: Expense[] | null | undefined;

  renderedIndex = 0;

  readonly expensesPerRender = 10;
  readonly skeletonItems = Array(10);

  ngOnChanges() {
    this.renderExpenses();
  }

  renderExpenses() {
    this.renderedIndex++;

    this.renderedExpenses = this.expenses?.slice(
      0,
      this.renderedIndex * this.expensesPerRender
    );
  }

  loadNext($event: Event) {
    const event = <InfiniteScrollCustomEvent>$event;

    this.renderExpenses();

    event.target.disabled =
      this.renderedExpenses?.length === this.expenses?.length;
    event.target.complete();
  }
}
