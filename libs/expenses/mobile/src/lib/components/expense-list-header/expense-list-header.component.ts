import { Component } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { ExpensesFilterComponent } from '../expenses-filter/expenses-filter.component';

@Component({
  selector: 'dinaro-expense-list-header',
  standalone: true,
  imports: [ExpensesFilterComponent, IonicModule, TranslateModule],
  templateUrl: './expense-list-header.component.html',
  styleUrls: ['./expense-list-header.component.scss'],
})
export class ExpenseListHeaderComponent {}
