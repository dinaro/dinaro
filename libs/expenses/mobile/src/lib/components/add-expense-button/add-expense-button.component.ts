import { Component } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { AddExpenseComponent } from '../add-expense/add-expense.component';

@Component({
  selector: 'dinaro-add-expense-button',
  standalone: true,
  imports: [AddExpenseComponent, IonicModule],
  templateUrl: 'add-expense-button.component.html',
  styleUrls: ['add-expense-button.component.scss'],
})
export class AddExpenseButtonComponent {}
