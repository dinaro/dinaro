import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {
  addExpense,
  deleteExpense,
  Expense,
  updateExpense,
} from '@dinaro/expenses';
import { IonicModule, ToastController } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'dinaro-edit-expense',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule,
  ],
  templateUrl: 'edit-expense.component.html',
  styleUrls: ['edit-expense.component.scss'],
})
export class EditExpenseComponent implements OnInit {
  @Input() expense!: Expense;
  @Output() readonly didSubmit = new EventEmitter<Expense>();
  @Output() readonly willDelete = new EventEmitter<Expense>();

  readonly formGroup = new FormGroup({
    amount: new FormControl<number | null>(null, Validators.required),
    date: new FormControl<string>('', Validators.required),
    description: new FormControl<string>(''),
  });

  constructor(private readonly toastController: ToastController) {}

  ngOnInit() {
    this.formGroup.patchValue({
      amount: this.expense.amount,
      date: this.expense.date,
      description: this.expense.description,
    });
  }

  submit() {
    const {
      dirty,
      invalid,
      value: { amount, date, description },
    } = this.formGroup;

    if (!dirty || invalid || !amount || !date) return;

    const expense: Partial<Expense> = {
      amount,
      date,
      description,
    };

    updateExpense(this.expense.id, expense);

    this.didSubmit.emit({ ...this.expense, ...expense });
  }

  deleteExpense() {
    this.willDelete.emit();

    deleteExpense(this.expense.id);

    this.presentExpenseDeletedToast();
  }

  async presentExpenseDeletedToast() {
    const toast = await this.toastController.create({
      header: 'Expense deleted',
      duration: 5000,
      buttons: [
        {
          handler: () => addExpense(this.expense),
          text: 'Undo',
        },
      ],
    });

    await toast.present();
  }
}
