import { Component, EventEmitter, Output } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { addExpense, Expense } from '@dinaro/expenses';
import { formatISO } from 'date-fns';
import * as cuid from 'cuid';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'dinaro-add-expense',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule,
  ],
  templateUrl: 'add-expense.component.html',
  styleUrls: ['add-expense.component.scss'],
})
export class AddExpenseComponent {
  @Output() readonly didSubmit = new EventEmitter<Expense>();

  readonly formGroup = new FormGroup({
    amount: new FormControl(null, Validators.required),
    date: new FormControl(formatISO(new Date()), Validators.required),
    description: new FormControl(''),
  });

  submit() {
    const {
      invalid,
      value: { amount, date, description },
    } = this.formGroup;

    if (invalid || !amount || !date) return;

    const expense: Expense = {
      id: cuid(),
      createdAt: formatISO(new Date()),
      amount,
      date,
      description,
    };

    addExpense(expense);

    this.didSubmit.emit(expense);
  }
}
