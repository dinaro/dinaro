import { Component, Input } from '@angular/core';
import { IonicModule } from '@ionic/angular';

@Component({
  standalone: true,
  imports: [IonicModule],
  selector: 'dinaro-data-card',
  templateUrl: 'data-card.component.html',
  styleUrls: ['data-card.component.scss'],
})
export class DataCardComponent {
  @Input() data: string | number | null | undefined;
  @Input() label: string | undefined;
}
