import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import {
  AddExpenseButtonComponent,
  ExpenseListComponent,
  ExpenseListHeaderComponent,
} from '@dinaro/expenses/mobile';
import { DataCardComponent } from '@dinaro/ui/mobile';

@NgModule({
  imports: [
    AddExpenseButtonComponent,
    CommonModule,
    DataCardComponent,
    ExpenseListComponent,
    ExpenseListHeaderComponent,
    IonicModule,
    HomeRoutingModule,
    TranslateModule,
  ],
  declarations: [HomeComponent],
})
export class HomeModule {}
